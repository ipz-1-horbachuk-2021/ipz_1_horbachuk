#include "pch.h"
#include "DB.h"

#pragma warning(disable: 4996)

SOCKET Connection[100];
int counter = 0;
enum  Request
{
	R_Login,
	R_Registration,
	R_SignInOnQueue1,
	R_SignInOnQueue2,
	R_SignInOnQueue3,
	R_ShowQueue,
	R_Change1,
	R_Change2,
	R_Change3
};																				//

void Login(int index, DB^ a) {
	int sizeofString;
	int type;
	recv(Connection[index], (char*)&sizeofString, sizeof(int), NULL);
	char* login = new char[sizeofString + 1];
	login[sizeofString] = '\0';
	recv(Connection[index], login, sizeofString, NULL);

	recv(Connection[index], (char*)&sizeofString, sizeof(int), NULL);
	char* password = new char[sizeofString + 1];
	password[sizeofString] = '\0';
	recv(Connection[index], password, sizeofString, NULL);
	int reqRes = a->Select(login, password, &type);
	send(Connection[index], (char*)&reqRes, sizeof(int), NULL);
	send(Connection[index], (char*)&type, sizeof(int), NULL);
}
void Registration(int index, DB^ a) {
	int sizeOfString;
	//name
	recv(Connection[index], (char*)&sizeOfString, sizeof(int), NULL);
	char* name = new char[sizeOfString + 1];
	name[sizeOfString] = '\0';
	recv(Connection[index], name, sizeOfString, NULL);
	//last name
	recv(Connection[index], (char*)&sizeOfString, sizeof(int), NULL);
	char* lastName = new char[sizeOfString + 1];
	lastName[sizeOfString] = '\0';
	recv(Connection[index], lastName, sizeOfString, NULL);
	//login
	recv(Connection[index], (char*)&sizeOfString, sizeof(int), NULL);
	char* login = new char[sizeOfString + 1];
	login[sizeOfString] = '\0';
	recv(Connection[index], login, sizeOfString, NULL);
	bool isLoginExist = a->ExistOfLogin(login);
	//password
	recv(Connection[index], (char*)&sizeOfString, sizeof(int), NULL);
	char* password = new char[sizeOfString + 1];
	password[sizeOfString] = '\0';
	recv(Connection[index], password, sizeOfString, NULL);
	//call func to insert data in db
	if (isLoginExist == false) {
		a->Insert(name, lastName, login, password);
	}
	send(Connection[index], (char*)&isLoginExist, sizeof(bool), NULL);
}
void SignInOnQueue1(int index, DB^ a) {
	int id;
	recv(Connection[index], (char*)&id, sizeof(int), NULL);
	a->SendNumInQueue1(Connection[index], id);
}
void SignInOnQueue2(int index, DB^ a) {
	int id;
	recv(Connection[index], (char*)&id, sizeof(int), NULL);
	a->SendNumInQueue2(Connection[index], id);
}
void SignInOnQueue3(int index, DB^ a) {
	int id;
	recv(Connection[index], (char*)&id, sizeof(int), NULL);
	a->SendNumInQueue3(Connection[index], id);
}
void ShowQueue(int index, DB^ a) {
	a->SendQueue(Connection[index]);
}
void Change1(int index, DB^ a) {
	a->ChangeQ1(Connection[index]);
}
void Change2(int index, DB^ a) {
	a->ChangeQ2(Connection[index]);
}
void Change3(int index, DB^ a) {
	a->ChangeQ3(Connection[index]);
}
void ClientHandler(int index) {
	DB^ DBO = gcnew DB();
	DBO->ConnectToDB();
	while (true) {
		Request req;
		recv(Connection[index], (char*)&req, sizeof(int), NULL);
		switch (req) {
			//Login
		case R_Login:
			Login(index, DBO);
			break;
			//Registration
		case R_Registration:
			Registration(index, DBO);
			break;
			//SignInOnQueue
		case R_SignInOnQueue1:
			SignInOnQueue1(index, DBO);
			break;
		case R_SignInOnQueue2:
			SignInOnQueue2(index, DBO);
			break;
		case R_SignInOnQueue3:
			SignInOnQueue3(index, DBO);
			break;
		case R_ShowQueue:
			ShowQueue(index, DBO);
			break;
		case R_Change1:
			Change1(index, DBO);
			break;
		case R_Change2:
			Change2(index, DBO);
			break;
		case R_Change3:
			Change3(index, DBO);
			break;
		}
	}
}
int main(int args, char* argv[])
{
	WSAData wsaData;
	WORD DLLVersion = MAKEWORD(2, 1);
	if (WSAStartup(DLLVersion, &wsaData) != 0)
	{
		std::cout << "Error" << std::endl;
		exit(1);
	}

	SOCKADDR_IN addr;
	int sizeOfAddr = sizeof(addr);
	//addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_addr.s_addr = inet_addr("0.0.0.0");
	addr.sin_port = htons(5000);
	addr.sin_family = AF_INET;

	SOCKET sListen = socket(AF_INET, SOCK_STREAM, NULL);
	bind(sListen, (SOCKADDR*)&addr, sizeof(addr));
	listen(sListen, SOMAXCONN);

	SOCKET newConnection;
	for (int i = 0; i < 100; i++) {


		newConnection = accept(sListen, (SOCKADDR*)&addr, &sizeOfAddr);

		if (newConnection == 0)
		{
			std::cout << "Error #2\n";
		}
		else
		{
			std::cout << "Client connected\n";
		}
		Connection[i] = newConnection;
		counter++;
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)ClientHandler, (LPVOID)(i), NULL, NULL);
	}
	system("pause");
	return 0;
}